﻿using Abstractions;
using System;
using System.Collections.Generic;

namespace Repository
{
    public interface IRepositorio<T> : ICrud<T>
    { 
    
    }
    public class Repositorio<T> : IRepositorio<T> where T : IEntity
    {
        IDbContext<T> _ctx;
        public Repositorio(IDbContext<T> ctx)
        {
            _ctx = ctx;
        }
        public void delete(int id)
        {
            _ctx.delete(id);
        }

        public IList<T> GetAll()
        {
            return _ctx.GetAll();
        }

        public T GetById(int id)
        {
            return _ctx.GetById(id);
        }

        public T InsertOrDelete(int id, T Entity)
        {
            return _ctx.InsertOrDelete(id, Entity);
        }

        public T Save(T Entity)
        {
            return _ctx.Save(Entity);
        }
    }
}
