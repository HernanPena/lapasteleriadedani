﻿using Entities;
using Entities.Auxiliares;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccess
{
    public class ApiDbContext : DbContext
    {
        public DbSet<FootballTeam> teams { get; set; }
        public DbSet<Producto> productos { get; set; }
        public DbSet<C_Categoria> categorias { get; set; }

        public ApiDbContext(DbContextOptions<ApiDbContext> options) : base(options)
        { 
        }

        protected  override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Ignore<Entity>();
            base.OnModelCreating(modelBuilder);
        }

    }
}
