﻿using Abstractions;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;

namespace DataAccess
{
    public class DbContext<T> : IDbContext<T> where T : class, IEntity
    {
        DbSet<T> _items;
        ApiDbContext _ctx;
        public DbContext(ApiDbContext ctx)
        {
            _ctx = ctx;
            _items = ctx.Set<T>();
        }
        public void delete(int id)
        {
           
        }

        public IList<T> GetAll()
        {
            return _items.ToList();
        }

        public T GetById(int id)
        {
            return _items.Where(i => i.id.Equals(id)).FirstOrDefault();
        }

        public T InsertOrDelete(int id, T Entity)
        {
            var o = GetById(id);
            if (o == null)
            {
                Save(Entity);
                return (Entity);
            }
            else
            {
                _items.Update(Entity);
                return (Entity);
            }
        }

        public T Save(T Entity)
        {
            _items.Add(Entity);
            _ctx.SaveChanges();
            return Entity;
        }

    }
}
