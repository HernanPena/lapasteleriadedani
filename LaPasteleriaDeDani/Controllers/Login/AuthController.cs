﻿using Api.Models.Auth;
using Api.Models.Response;
using Api.Servicios;
using LaPasteleriaDeDani.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Api.Controllers.Login
{
    [Route("api/[controller]")]
    [ApiController]
    public class AuthController : ControllerBase
    {
        private IUserService _userService;
        public AuthController(IUserService userService)
        {
            _userService = userService;
        }
        [HttpPost("login")]
        public IActionResult Autentificar([FromBody] LoginAuth model)
        {
            Respuesta respuesta = new Respuesta();
            var userresponse = _userService.Auth(model);
            if (userresponse == null)
            {
                respuesta.exito = 0;
                respuesta.mensaje = "usuario o contrasena incorrecta";
                return BadRequest(respuesta);
            
            }
            respuesta.exito = 1;
            respuesta.data = userresponse;
            return Ok(respuesta);
        }

    }
}
