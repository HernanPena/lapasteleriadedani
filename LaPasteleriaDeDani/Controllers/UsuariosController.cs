﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using LaPasteleriaDeDani.Data;
using LaPasteleriaDeDani.Models;
using Microsoft.AspNetCore.Authorization;

namespace ApiPasteleria.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class UsuariosController : ControllerBase
    {
        private readonly ContextDB _context;

        public UsuariosController(ContextDB context)
        {
            _context = context;
        }

        // Devuelve todos los usuarios activos 
        [HttpGet]
        public ActionResult<IEnumerable<Usuario>> Get()
        {
            try 
            {
                IEnumerable<Usuario> usuarios = (from u in _context.usuarios
                                                 where u.activo == true
                                                 orderby u.UsuarioID
                                                 select new  Usuario
                                                 {
                                                     UsuarioID = u.UsuarioID,
                                                     nombre = u.nombre,
                                                     apellido = u.apellido,
                                                     email = u.email,
                                                     id_tipo_documento = u.id_tipo_documento,
                                                     numero_documento = u.numero_documento,
                                                     telefono = u.telefono,
                                                     password = u.password,
                                                     fecha_alta = u.fecha_alta,
                                                     activo = u.activo
                                                 }
                                                 ).ToList();
                return usuarios.ToList();
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
        }

        // Devuelve un usuario segun ID
        [HttpGet("{id}")]
        public ActionResult<Usuario> Get(int id)
        {
            try
            {
                Usuario usuario = _context.usuarios.Find(id);
                if (usuario != null)
                {
                    return usuario;
                }
                else
                {
                    return NotFound();
                }
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
        }

        //Actualiza un usuario segun id
       [HttpPut("{id}")]
        public ActionResult Put(int id, Usuario usuario)
        {
            try
            {
                if (id == usuario.UsuarioID)
                {
                    var idbd = _context.usuarios.Where(a => a.UsuarioID == id).Count();

                    if (idbd > 0)
                    {
                        Usuario u = _context.usuarios.Find(id);
                        

                        //logica para que solo el usuario pueda modificar sus datos..

                    }
                }
                else
                {
                    return BadRequest();

                }
            }
            catch
            {

            }

            if (id != usuario.UsuarioID)
            {
                return BadRequest();
            }

            _context.Entry(usuario).State = EntityState.Modified;

            try
            {
                _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!UsuarioExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Usuarios
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPost]
        public async Task<ActionResult<Usuario>> PostUsuario(Usuario usuario)
        {
            _context.usuarios.Add(usuario);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetUsuario", new { id = usuario.UsuarioID }, usuario);
        }

        // DELETE: api/Usuarios/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteUsuario(int id)
        {
            var usuario = await _context.usuarios.FindAsync(id);
            if (usuario == null)
            {
                return NotFound();
            }

            _context.usuarios.Remove(usuario);
            await _context.SaveChangesAsync();

            return NoContent();
        }

        private bool UsuarioExists(int id)
        {
            return _context.usuarios.Any(e => e.UsuarioID == id);
        }
    }
}
