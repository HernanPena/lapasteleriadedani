﻿using Api.Models;
using Api.Models.Auth;
using Api.Models.Common;
using Api.Models.Response;
using Api.Tools;
using LaPasteleriaDeDani.Data;
using LaPasteleriaDeDani.Models;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace Api.Servicios
{
    public class UserService : IUserService
    {
        private readonly AppSettings _appSetings;
        public UserService(IOptions<AppSettings> appSettings)
        {
            _appSetings = appSettings.Value;
        }
        public UserResponse Auth(LoginAuth model)
        {
            UserResponse userresponse = new UserResponse();
            using (var db = new ContextDB())
            {
                string spassword = Encrypt.GetSHA256(model.password);
                var user = db.usuarios.Where(d => d.email == model.email && d.password == spassword).FirstOrDefault();
                if (user == null)
                {   
                    return null;
                }
                var rol = db.roles.Find(user.rol_id);
                userresponse.email = user.email;
                userresponse.token = GetToken(user, rol);

            }
            return userresponse;
        }
        private string GetToken(Usuario usuario, C_Roles rol)
        {
            
            var tokenHandler = new JwtSecurityTokenHandler();
            var llave = Encoding.ASCII.GetBytes(_appSetings.secreto);

            var tokenDescritor = new SecurityTokenDescriptor
            {
                Subject = new ClaimsIdentity(
                    new Claim[]
                    {
                        new Claim("id_usuario", usuario.UsuarioID.ToString()),
                        new Claim(ClaimTypes.NameIdentifier, usuario.email),
                        new Claim(ClaimTypes.Role, rol.rol)
                    }
                    ),
                Expires = DateTime.UtcNow.AddMinutes(60),
                SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(llave), SecurityAlgorithms.HmacSha256Signature)
            };
            var token = tokenHandler.CreateToken(tokenDescritor);
            return tokenHandler.WriteToken(token);
        }
    }
    
}
