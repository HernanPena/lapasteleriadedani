﻿using Api.Models.Auth;
using Api.Models.Response;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Api.Servicios
{
    public interface IUserService
    {
        UserResponse Auth(LoginAuth model);
    }
}
