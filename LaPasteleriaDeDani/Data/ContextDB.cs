﻿using Api.Models;
using LaPasteleriaDeDani.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;

namespace LaPasteleriaDeDani.Data
{
    public partial class ContextDB : DbContext
    {
        public ContextDB()
        {

        }
        public ContextDB(DbContextOptions<ContextDB> options) : base(options)
        {

        }
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
                optionsBuilder.UseSqlServer("Data Source=BLACKHORDE\\BLACKHORDE;Persist Security Info=True;User ID=sa;Initial Catalog=LaPasteleriaDeDani; Password=34671716;");
            }

        }
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            foreach (var relationship in modelBuilder.Model.GetEntityTypes().SelectMany(e => e.GetForeignKeys()))
            {
                relationship.DeleteBehavior = DeleteBehavior.Restrict;
            }
            base.OnModelCreating(modelBuilder);
            modelBuilder.HasAnnotation("Relational:Collation", "SQL_Latin1_General_CP1_CI_AS");
            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);

        public DbSet<C_Tipo_Documento> tiposdocumentos { set; get; }
        public DbSet<C_Estado_Orden> estadoordenes { get; set; }
        public DbSet<C_Provincia> provincias { get; set; }
        public DbSet<C_Metodo_Pago> metodopagos { get; set; }
        public DbSet<C_Localidad> localidades { get; set; }
        public DbSet<C_Roles> roles { get; set; }
        public DbSet<C_Categoria> categorias { get; set; }
        public DbSet<Usuario> usuarios { get; set; }
        public DbSet<Direccion> direcciones { get; set; }
        public DbSet<Producto> productos { get; set; }
        public DbSet<Carrito> carritos { get; set; }
        public DbSet<OrdenDeCompra> ordenesdecompra { get; set; }
        public DbSet<DetalleOrdenDeCompra> detalleOrdenDeCompras { get; set; }
        public DbSet<Pago> pagos { get; set; }
        public DbSet <DetallePago> detallePagos { get; set; }
    }
}
