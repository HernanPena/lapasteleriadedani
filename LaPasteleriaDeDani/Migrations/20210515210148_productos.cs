﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace LaPasteleriaDeDani.Migrations
{
    public partial class productos : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_direcciones_localidad_id_localidad",
                table: "direcciones");

            migrationBuilder.DropForeignKey(
                name: "FK_localidad_provincias_id_provincia",
                table: "localidad");

            migrationBuilder.DropForeignKey(
                name: "FK_usuarios_TipoDocumento_id_tipo_documento",
                table: "usuarios");

            migrationBuilder.DropPrimaryKey(
                name: "PK_TipoDocumento",
                table: "TipoDocumento");

            migrationBuilder.DropPrimaryKey(
                name: "PK_localidad",
                table: "localidad");

            migrationBuilder.RenameTable(
                name: "TipoDocumento",
                newName: "tiposdocumentos");

            migrationBuilder.RenameTable(
                name: "localidad",
                newName: "localidades");

            migrationBuilder.RenameIndex(
                name: "IX_localidad_id_provincia",
                table: "localidades",
                newName: "IX_localidades_id_provincia");

            migrationBuilder.AddPrimaryKey(
                name: "PK_tiposdocumentos",
                table: "tiposdocumentos",
                column: "C_Tipo_DocumentoID");

            migrationBuilder.AddPrimaryKey(
                name: "PK_localidades",
                table: "localidades",
                column: "id");

            migrationBuilder.CreateTable(
                name: "categorias",
                columns: table => new
                {
                    id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    categoria = table.Column<string>(type: "nvarchar(10)", maxLength: 10, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_categorias", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "productos",
                columns: table => new
                {
                    id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    nombre = table.Column<string>(type: "nvarchar(20)", maxLength: 20, nullable: false),
                    descripcion = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: false),
                    precio = table.Column<double>(type: "float", nullable: false),
                    id_categoria = table.Column<int>(type: "int", nullable: false),
                    urlImagen = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    stock = table.Column<int>(type: "int", nullable: false),
                    activo = table.Column<bool>(type: "bit", nullable: false),
                    fecha_alta = table.Column<DateTime>(type: "datetime2", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_productos", x => x.id);
                    table.ForeignKey(
                        name: "FK_productos_categorias_id_categoria",
                        column: x => x.id_categoria,
                        principalTable: "categorias",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_productos_id_categoria",
                table: "productos",
                column: "id_categoria");

            migrationBuilder.AddForeignKey(
                name: "FK_direcciones_localidades_id_localidad",
                table: "direcciones",
                column: "id_localidad",
                principalTable: "localidades",
                principalColumn: "id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_localidades_provincias_id_provincia",
                table: "localidades",
                column: "id_provincia",
                principalTable: "provincias",
                principalColumn: "id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_usuarios_tiposdocumentos_id_tipo_documento",
                table: "usuarios",
                column: "id_tipo_documento",
                principalTable: "tiposdocumentos",
                principalColumn: "C_Tipo_DocumentoID",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_direcciones_localidades_id_localidad",
                table: "direcciones");

            migrationBuilder.DropForeignKey(
                name: "FK_localidades_provincias_id_provincia",
                table: "localidades");

            migrationBuilder.DropForeignKey(
                name: "FK_usuarios_tiposdocumentos_id_tipo_documento",
                table: "usuarios");

            migrationBuilder.DropTable(
                name: "productos");

            migrationBuilder.DropTable(
                name: "categorias");

            migrationBuilder.DropPrimaryKey(
                name: "PK_tiposdocumentos",
                table: "tiposdocumentos");

            migrationBuilder.DropPrimaryKey(
                name: "PK_localidades",
                table: "localidades");

            migrationBuilder.RenameTable(
                name: "tiposdocumentos",
                newName: "TipoDocumento");

            migrationBuilder.RenameTable(
                name: "localidades",
                newName: "localidad");

            migrationBuilder.RenameIndex(
                name: "IX_localidades_id_provincia",
                table: "localidad",
                newName: "IX_localidad_id_provincia");

            migrationBuilder.AddPrimaryKey(
                name: "PK_TipoDocumento",
                table: "TipoDocumento",
                column: "C_Tipo_DocumentoID");

            migrationBuilder.AddPrimaryKey(
                name: "PK_localidad",
                table: "localidad",
                column: "id");

            migrationBuilder.AddForeignKey(
                name: "FK_direcciones_localidad_id_localidad",
                table: "direcciones",
                column: "id_localidad",
                principalTable: "localidad",
                principalColumn: "id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_localidad_provincias_id_provincia",
                table: "localidad",
                column: "id_provincia",
                principalTable: "provincias",
                principalColumn: "id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_usuarios_TipoDocumento_id_tipo_documento",
                table: "usuarios",
                column: "id_tipo_documento",
                principalTable: "TipoDocumento",
                principalColumn: "C_Tipo_DocumentoID",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
