﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace LaPasteleriaDeDani.Migrations
{
    public partial class pagos : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "id_pago",
                table: "ordenesdecompra",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.CreateTable(
                name: "metodopagos",
                columns: table => new
                {
                    id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    metodo = table.Column<string>(type: "nvarchar(15)", maxLength: 15, nullable: false),
                    detalles_generales = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    detalle_transferencia = table.Column<string>(type: "nvarchar(max)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_metodopagos", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "pagos",
                columns: table => new
                {
                    id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    id_orden_compra = table.Column<int>(type: "int", nullable: false),
                    fecha = table.Column<DateTime>(type: "datetime2", nullable: false),
                    total = table.Column<double>(type: "float", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_pagos", x => x.id);
                    table.ForeignKey(
                        name: "FK_pagos_ordenesdecompra_id_orden_compra",
                        column: x => x.id_orden_compra,
                        principalTable: "ordenesdecompra",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "detallePagos",
                columns: table => new
                {
                    id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    id_pago = table.Column<int>(type: "int", nullable: false),
                    id_metodo_pago = table.Column<int>(type: "int", nullable: false),
                    pago_realizado = table.Column<double>(type: "float", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_detallePagos", x => x.id);
                    table.ForeignKey(
                        name: "FK_detallePagos_metodopagos_id_metodo_pago",
                        column: x => x.id_metodo_pago,
                        principalTable: "metodopagos",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_detallePagos_pagos_id_pago",
                        column: x => x.id_pago,
                        principalTable: "pagos",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_ordenesdecompra_id_pago",
                table: "ordenesdecompra",
                column: "id_pago");

            migrationBuilder.CreateIndex(
                name: "IX_detallePagos_id_metodo_pago",
                table: "detallePagos",
                column: "id_metodo_pago");

            migrationBuilder.CreateIndex(
                name: "IX_detallePagos_id_pago",
                table: "detallePagos",
                column: "id_pago");

            migrationBuilder.CreateIndex(
                name: "IX_pagos_id_orden_compra",
                table: "pagos",
                column: "id_orden_compra");

            migrationBuilder.AddForeignKey(
                name: "FK_ordenesdecompra_pagos_id_pago",
                table: "ordenesdecompra",
                column: "id_pago",
                principalTable: "pagos",
                principalColumn: "id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_ordenesdecompra_pagos_id_pago",
                table: "ordenesdecompra");

            migrationBuilder.DropTable(
                name: "detallePagos");

            migrationBuilder.DropTable(
                name: "metodopagos");

            migrationBuilder.DropTable(
                name: "pagos");

            migrationBuilder.DropIndex(
                name: "IX_ordenesdecompra_id_pago",
                table: "ordenesdecompra");

            migrationBuilder.DropColumn(
                name: "id_pago",
                table: "ordenesdecompra");
        }
    }
}
