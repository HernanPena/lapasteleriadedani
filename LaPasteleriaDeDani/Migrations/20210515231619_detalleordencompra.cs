﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace LaPasteleriaDeDani.Migrations
{
    public partial class detalleordencompra : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "detalleOrdenDeCompras",
                columns: table => new
                {
                    id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    id_orden_compra = table.Column<int>(type: "int", nullable: false),
                    id_producto = table.Column<int>(type: "int", nullable: false),
                    cantidad = table.Column<int>(type: "int", nullable: false),
                    precioUnitario = table.Column<double>(type: "float", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_detalleOrdenDeCompras", x => x.id);
                    table.ForeignKey(
                        name: "FK_detalleOrdenDeCompras_ordenesdecompra_id_orden_compra",
                        column: x => x.id_orden_compra,
                        principalTable: "ordenesdecompra",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_detalleOrdenDeCompras_productos_id_producto",
                        column: x => x.id_producto,
                        principalTable: "productos",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_detalleOrdenDeCompras_id_orden_compra",
                table: "detalleOrdenDeCompras",
                column: "id_orden_compra");

            migrationBuilder.CreateIndex(
                name: "IX_detalleOrdenDeCompras_id_producto",
                table: "detalleOrdenDeCompras",
                column: "id_producto");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "detalleOrdenDeCompras");
        }
    }
}
