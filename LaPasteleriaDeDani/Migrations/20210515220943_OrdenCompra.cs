﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace LaPasteleriaDeDani.Migrations
{
    public partial class OrdenCompra : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_direcciones_localidades_id_localidad",
                table: "direcciones");

            migrationBuilder.DropForeignKey(
                name: "FK_direcciones_usuarios_id_usuario",
                table: "direcciones");

            migrationBuilder.DropForeignKey(
                name: "FK_localidades_provincias_id_provincia",
                table: "localidades");

            migrationBuilder.DropForeignKey(
                name: "FK_productos_categorias_id_categoria",
                table: "productos");

            migrationBuilder.DropForeignKey(
                name: "FK_usuarios_tiposdocumentos_id_tipo_documento",
                table: "usuarios");

            migrationBuilder.CreateTable(
                name: "carritos",
                columns: table => new
                {
                    id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    id_usuario = table.Column<int>(type: "int", nullable: false),
                    id_producto = table.Column<int>(type: "int", nullable: false),
                    cantidad = table.Column<int>(type: "int", nullable: false),
                    estado = table.Column<bool>(type: "bit", nullable: false),
                    fecha_alta = table.Column<DateTime>(type: "datetime2", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_carritos", x => x.id);
                    table.ForeignKey(
                        name: "FK_carritos_productos_id_producto",
                        column: x => x.id_producto,
                        principalTable: "productos",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_carritos_usuarios_id_usuario",
                        column: x => x.id_usuario,
                        principalTable: "usuarios",
                        principalColumn: "UsuarioID",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "estadoordenes",
                columns: table => new
                {
                    id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    estado = table.Column<string>(type: "nvarchar(20)", maxLength: 20, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_estadoordenes", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "ordenesdecompra",
                columns: table => new
                {
                    id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    id_usuario = table.Column<int>(type: "int", nullable: false),
                    id_direccion = table.Column<int>(type: "int", nullable: false),
                    total = table.Column<double>(type: "float", nullable: false),
                    id_estado_orden = table.Column<int>(type: "int", nullable: false),
                    fecha_alta = table.Column<DateTime>(type: "datetime2", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ordenesdecompra", x => x.id);
                    table.ForeignKey(
                        name: "FK_ordenesdecompra_direcciones_id_direccion",
                        column: x => x.id_direccion,
                        principalTable: "direcciones",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_ordenesdecompra_estadoordenes_id_estado_orden",
                        column: x => x.id_estado_orden,
                        principalTable: "estadoordenes",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_ordenesdecompra_usuarios_id_usuario",
                        column: x => x.id_usuario,
                        principalTable: "usuarios",
                        principalColumn: "UsuarioID",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_carritos_id_producto",
                table: "carritos",
                column: "id_producto");

            migrationBuilder.CreateIndex(
                name: "IX_carritos_id_usuario",
                table: "carritos",
                column: "id_usuario");

            migrationBuilder.CreateIndex(
                name: "IX_ordenesdecompra_id_direccion",
                table: "ordenesdecompra",
                column: "id_direccion");

            migrationBuilder.CreateIndex(
                name: "IX_ordenesdecompra_id_estado_orden",
                table: "ordenesdecompra",
                column: "id_estado_orden");

            migrationBuilder.CreateIndex(
                name: "IX_ordenesdecompra_id_usuario",
                table: "ordenesdecompra",
                column: "id_usuario");

            migrationBuilder.AddForeignKey(
                name: "FK_direcciones_localidades_id_localidad",
                table: "direcciones",
                column: "id_localidad",
                principalTable: "localidades",
                principalColumn: "id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_direcciones_usuarios_id_usuario",
                table: "direcciones",
                column: "id_usuario",
                principalTable: "usuarios",
                principalColumn: "UsuarioID",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_localidades_provincias_id_provincia",
                table: "localidades",
                column: "id_provincia",
                principalTable: "provincias",
                principalColumn: "id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_productos_categorias_id_categoria",
                table: "productos",
                column: "id_categoria",
                principalTable: "categorias",
                principalColumn: "id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_usuarios_tiposdocumentos_id_tipo_documento",
                table: "usuarios",
                column: "id_tipo_documento",
                principalTable: "tiposdocumentos",
                principalColumn: "C_Tipo_DocumentoID",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_direcciones_localidades_id_localidad",
                table: "direcciones");

            migrationBuilder.DropForeignKey(
                name: "FK_direcciones_usuarios_id_usuario",
                table: "direcciones");

            migrationBuilder.DropForeignKey(
                name: "FK_localidades_provincias_id_provincia",
                table: "localidades");

            migrationBuilder.DropForeignKey(
                name: "FK_productos_categorias_id_categoria",
                table: "productos");

            migrationBuilder.DropForeignKey(
                name: "FK_usuarios_tiposdocumentos_id_tipo_documento",
                table: "usuarios");

            migrationBuilder.DropTable(
                name: "carritos");

            migrationBuilder.DropTable(
                name: "ordenesdecompra");

            migrationBuilder.DropTable(
                name: "estadoordenes");

            migrationBuilder.AddForeignKey(
                name: "FK_direcciones_localidades_id_localidad",
                table: "direcciones",
                column: "id_localidad",
                principalTable: "localidades",
                principalColumn: "id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_direcciones_usuarios_id_usuario",
                table: "direcciones",
                column: "id_usuario",
                principalTable: "usuarios",
                principalColumn: "UsuarioID",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_localidades_provincias_id_provincia",
                table: "localidades",
                column: "id_provincia",
                principalTable: "provincias",
                principalColumn: "id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_productos_categorias_id_categoria",
                table: "productos",
                column: "id_categoria",
                principalTable: "categorias",
                principalColumn: "id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_usuarios_tiposdocumentos_id_tipo_documento",
                table: "usuarios",
                column: "id_tipo_documento",
                principalTable: "tiposdocumentos",
                principalColumn: "C_Tipo_DocumentoID",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
