﻿
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
namespace LaPasteleriaDeDani.Models
{
    public class C_Provincia
    {
        [Key]
        public int id { get; set; }


        [Required(AllowEmptyStrings =false, ErrorMessage ="Se necesita especificar la provincia")]
        public string provincia { get; set; }
    }
}