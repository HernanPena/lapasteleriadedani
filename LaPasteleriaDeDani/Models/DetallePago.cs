﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace LaPasteleriaDeDani.Models
{
    public class DetallePago
    {
        [Key]
        public int id { get; set; }


        [Required(AllowEmptyStrings =false, ErrorMessage ="Se requiere un pago asociado")]
        public int id_pago { get; set; }


        [Required]
        [ForeignKey("id_pago")]
        public Pago pago { get; set; }


        [Required(AllowEmptyStrings =false, ErrorMessage ="Se requiere un metodo de pago")]
        public int id_metodo_pago { get; set; }


        [Required]
        [ForeignKey("id_metodo_pago")]
        public C_Metodo_Pago metodo_pago { get; set; }


        [Required(AllowEmptyStrings =false, ErrorMessage ="Se requiere la cantidad pagada")]
        public double pago_realizado { get; set; }

    }
}
