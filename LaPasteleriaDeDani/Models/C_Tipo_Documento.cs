﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace LaPasteleriaDeDani.Models
{
    public class C_Tipo_Documento
    {
        [Key]
        public int C_Tipo_DocumentoID { get; set; }

        [Required(AllowEmptyStrings =false, ErrorMessage ="Se requiere especificar el tipo de documento")]
        [MinLength(2, ErrorMessage = "Requiere como minimo 2 caracteres")]
        [MaxLength(10, ErrorMessage = "El maximo son 10 caracteres")]
        public string tipo_documento { get; set; }
    }
}
