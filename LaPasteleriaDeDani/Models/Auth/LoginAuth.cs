﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Api.Models.Auth
{
    public class LoginAuth
    {
        [Required(AllowEmptyStrings =false, ErrorMessage = "Se requiere el correo")]
        [DataType(DataType.EmailAddress)]
        public string email { get; set; }
        [Required(AllowEmptyStrings =false, ErrorMessage ="El password es obligatorio")]
        public string password { get; set; }
    }
}
