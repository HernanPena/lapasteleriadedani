﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;


namespace LaPasteleriaDeDani.Models
{
    public class Direccion
    {
        [Key]
        public int Id { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = "Se requiere un usuario")]
        public int id_usuario { get; set; }

        [Required]
        [ForeignKey("id_usuario")]
        public Usuario usuario { get; set; }
        
        [Required(AllowEmptyStrings =false, ErrorMessage ="Se requiere el nombre de la calle")]
        [StringLength(20,ErrorMessage ="El maximo son 20 caracteres")]
        public string calle { get; set; }
        
        [RegularExpression("(^[0-9]+$)", ErrorMessage = "Solo se permiten numeros")]
        [MinLength(2, ErrorMessage = "Requiere como minimo 2 caracteres")]
        [MaxLength(5, ErrorMessage = "El maximo son 5 caracteres")]
        public int numero { get; set; }

        
        [Required(AllowEmptyStrings =false, ErrorMessage ="Se requiere una localidad")]
        public int id_localidad { get; set; }


        [Required]
        [ForeignKey("id_localidad")]
        public C_Localidad localidad { get; set; }


        [Required(AllowEmptyStrings =false, ErrorMessage ="Codigo postal obligatorio")]
        [RegularExpression("(^[0-9]+$)", ErrorMessage = "Solo se permiten numeros")]
        public int codigo_postal { get; set; }
    }
}
