﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Api.Models
{
    public class C_Roles
    {
        [Key]
        public int id { get; set; }

        [Required(AllowEmptyStrings =false, ErrorMessage ="Se requiere una descripcion del rol")]
        [MinLength(2, ErrorMessage = "Requiere como minimo 2 caracteres")]
        [MaxLength(20, ErrorMessage = "El maximo son 20 caracteres")]
        public string rol { get; set; }
    }
}
