﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;


namespace LaPasteleriaDeDani.Models
{
    public class C_Estado_Orden
    {
        [Key]
        public int id { get; set; }
        [Required(AllowEmptyStrings =false, ErrorMessage ="Se requiere descripcion del estado de la orden")]
        [MinLength(2, ErrorMessage = "Requiere como minimo 2 caracteres")]
        [MaxLength(20, ErrorMessage = "El maximo son 20 caracteres")]
        public string estado { get; set; }
    }
}
