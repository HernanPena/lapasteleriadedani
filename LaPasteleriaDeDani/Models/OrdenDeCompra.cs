﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace LaPasteleriaDeDani.Models
{
    public class OrdenDeCompra
    {
        [Key]
        public int id { get; set; }


        [Required(AllowEmptyStrings =false, ErrorMessage ="Se requiere un usuario")]
        public int id_usuario { get; set; }
        

        [Required]
        [ForeignKey("id_usuario")]
        public Usuario usuario { get; set;  }


        [Required(AllowEmptyStrings =false, ErrorMessage ="Se requiere una direccion")]
        public int id_direccion { get; set; }

        
        [Required]
        [ForeignKey("id_direccion")]
        public Direccion direccion { get; set; }


        [Required(AllowEmptyStrings =false, ErrorMessage ="Se requiere cargar un pago")]
        public int id_pago { get; set; }


        [Required]
        [ForeignKey("id_pago")]
        public Pago pago { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = "Es necesiario el total para la orden")]
        [DataType(DataType.Currency)]
        [Range(0.0, double.MaxValue, ErrorMessage = "El valor es demasiado grande")]
        public double total { get; set; }


        [Required(AllowEmptyStrings =false, ErrorMessage ="Se requiere especificar el estado de la orden")]
        public int id_estado_orden { get; set; }
        

        [Required]
        [ForeignKey("id_estado_orden")]
        public C_Estado_Orden estado_orden { get; set; }


        [Required]
        [DataType(DataType.Date)]
        public DateTime fecha_alta { get; set; }

    }
}
