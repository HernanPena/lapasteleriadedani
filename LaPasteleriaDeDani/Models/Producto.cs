﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace LaPasteleriaDeDani.Models
{
    public class Producto
    {
        [Key]
        public int id { get; set; }


        [Required(AllowEmptyStrings = false, ErrorMessage ="Se require un nombre de producto")]
        [MinLength(2, ErrorMessage = "Requiere como minimo 2 caracteres")]
        [MaxLength(20, ErrorMessage = "El maximo son 20 caracteres")]
        public string nombre { get; set; }


        [Required(AllowEmptyStrings =false, ErrorMessage ="Se requiere una descripcion para el producto")]
        [MinLength(2, ErrorMessage = "Requiere como minimo 2 caracteres")]
        [MaxLength(50, ErrorMessage = "El maximo son 50 caracteres")]
        public string descripcion { get; set; }


        [Required(AllowEmptyStrings =false, ErrorMessage ="Es necesiario un precio para el producto")]
        [DataType(DataType.Currency)]
        [Range(0.0,double.MaxValue,ErrorMessage ="El valor es demasiado grande")]
        public double precio { get; set; }


        [Required(AllowEmptyStrings =false, ErrorMessage ="Se requiere la categoria del producto")]
        public int id_categoria { get; set; }


        [Required]
        [ForeignKey("id_categoria")]
        public C_Categoria categoria { get; set; }


        [Required]
        public string urlImagen { get; set; }


        [Required]
        [Range(1, int.MaxValue, ErrorMessage ="El valor es demasiado grande")]
        public int stock { get; set; }


        [Required]
        public bool activo { get; set; }


        [Required]
        [DataType(DataType.Date)]
        public DateTime fecha_alta { get; set; }
    }
}
