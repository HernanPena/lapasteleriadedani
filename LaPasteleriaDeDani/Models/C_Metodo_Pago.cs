﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace LaPasteleriaDeDani.Models
{
    public class C_Metodo_Pago
    {
        [Key]
        public int id { get; set; }


        [Required(AllowEmptyStrings = false, ErrorMessage = "Se requiere escribir el nombre de la localidad")]
        [MinLength(2, ErrorMessage = "Requiere como minimo 2 caracteres")]
        [MaxLength(15, ErrorMessage = "El maximo son 15 caracteres")]
        public string metodo { get; set; }
        public string detalles_generales { get; set; }
        //public string N_tarjeta_credito { get; set; }
        //public string N_tarjeta_debito { get; set; }
        public string detalle_transferencia { get; set; }

    }
}
