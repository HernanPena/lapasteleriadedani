﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace LaPasteleriaDeDani.Models
{
    public class Pago
    {
        [Key]
        public int id { get; set; }


        [Required(AllowEmptyStrings =false, ErrorMessage ="Se requiere una orden de compra asociada")]
        public int id_orden_compra { get; set; }


        [Required]
        [ForeignKey("id_orden_compra")]
        public OrdenDeCompra ordenDeCompra { get; set; }


        [Required]
        [DataType(DataType.Date)]
        public DateTime fecha { get; set; }


        [Required(AllowEmptyStrings = false, ErrorMessage = "Es necesiario un precio para el producto")]
        [DataType(DataType.Currency)]
        [Range(0.0, double.MaxValue, ErrorMessage = "El valor es demasiado grande")]
        public double total { get; set; }

    }
}
