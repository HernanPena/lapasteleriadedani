﻿using System;
using System.Collections.Generic;

namespace Abstractions
{
    public interface ICrud <T>
    {
        T Save(T Entity);
        IList<T> GetAll();
        T GetById(int id);
        void delete(int id);

        T InsertOrDelete(int id, T Entity);

    }
}
