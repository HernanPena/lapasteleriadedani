﻿using Application;
using Entities;
using Entities.Auxiliares;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebApi.DTOs;

namespace WebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ProductoController : ControllerBase
    {
        IApplicacion<Producto> _producto;
        IApplicacion<C_Categoria> _categoria;
        public ProductoController(IApplicacion<Producto> producto, IApplicacion<C_Categoria> categoria )
        {
            _producto = producto;
            _categoria = categoria;
        }
        [HttpGet]
        public IActionResult get()
        {
            return Ok(_producto.GetAll());
        }
        [HttpGet("{id}")]
        public IActionResult get(int id)
        {
            return Ok(_producto.GetById(id));
        }
        [HttpPost]
        public IActionResult save(ProductoDTO dto)
        {
            var p = new Producto()
            { 
                nombre = dto.nombre,
                descripcion = dto.descripcion,
                id_categoria = dto.id_categoria,
                fecha_alta = dto.fecha_alta,
                precio = dto.precio,
                urlImagen = dto.urlImagen,
                stock = dto.stock,
                activo = dto.activo
                
            };
            return Ok(_producto.Save(p));
        }
        [HttpDelete("{id}")]
        public IActionResult delete(int id)
        {
            Producto p = (Producto)_producto.GetById(id);

            if (p != null)
            {
                p.activo = false;
                C_Categoria categoria = (C_Categoria) _categoria.GetById(p.id_categoria);
                p.categoria = categoria;
                return Ok(_producto.InsertOrDelete(id,p));
            }
            else
                return BadRequest(error: "Error");
        }

    }
}
