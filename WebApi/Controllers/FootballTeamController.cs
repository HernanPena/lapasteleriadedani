﻿using Application;
using Entities;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebApi.DTOs;

namespace WebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class FootballTeamController : ControllerBase
    {
        IApplicacion<FootballTeam> _footballteam;
        public FootballTeamController(IApplicacion<FootballTeam> footballteam)
        {
            _footballteam = footballteam;
        }
        [HttpGet]

        public IActionResult get()
        {
            return Ok(_footballteam.GetAll());
        }
        [HttpPost]
        public IActionResult save(FootballTeamDTO dto)
        {
            var f = new FootballTeam()
            { 
                nombre = dto.name,
                score = dto.score,
                manager = dto.manager
                
            };
            return Ok(_footballteam.Save(f));

        }

    }
}
