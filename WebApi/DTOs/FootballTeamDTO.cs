﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebApi.DTOs
{
    public class FootballTeamDTO
    {
        public string name { get; set; }
        public double score { get; set; }
        public string manager { get; set; }
    }
}
