﻿using System.ComponentModel.DataAnnotations;

namespace WebApi.DTOs
{
    public class C_CategoriaDTO
    {

        [Required(AllowEmptyStrings = false, ErrorMessage = "Se requiere escribir el nombre de la localidad")]
        [MinLength(2, ErrorMessage = "Requiere como minimo 2 caracteres")]
        [MaxLength(10, ErrorMessage = "El maximo son 10 caracteres")]
        public string categoria { get; set; }
    }
}