﻿using Abstractions;
using Repository;
using System;
using System.Collections.Generic;

namespace Application
{
    public interface IApplicacion<T>:ICrud<T>
    { 
    
    }
    public class Aplicacion<T> : IApplicacion<T> where T: IEntity
    {
        IRepositorio<T> _repositorio;
        public Aplicacion(IRepositorio<T> repositorio)
        {
            _repositorio = repositorio;
        }
        public void delete(int id)
        {
            _repositorio.delete(id);
        }

        public IList<T> GetAll()
        {
            return _repositorio.GetAll();
        }

        public T GetById(int id)
        {
            return _repositorio.GetById(id);
        }

        public T InsertOrDelete(int id, T Entity)
        {
            return _repositorio.InsertOrDelete(id, Entity);
        }

        public T Save(T Entity)
        {
            return _repositorio.Save(Entity);
        }
    }
}
