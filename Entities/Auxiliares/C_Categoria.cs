﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entities.Auxiliares
{
    public class C_Categoria : Entity
    {
        [Required(AllowEmptyStrings = false, ErrorMessage = "Se requiere escribir el nombre de la localidad")]
        [MinLength(2, ErrorMessage = "Requiere como minimo 2 caracteres")]
        [MaxLength(10, ErrorMessage = "El maximo son 10 caracteres")]
        public string categoria { get; set; }
    }
}
