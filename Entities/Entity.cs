﻿using Abstractions;

namespace Entities
{
    public abstract class Entity : IEntity
    {
        public int id { get; set; }
    }
}
