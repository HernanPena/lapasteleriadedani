﻿using System;

namespace Entities
{
    public class FootballTeam : Entity
    {
        public string nombre { get; set; }
        public double score { get; set; }
        public string manager { get; set; }
    }
}
